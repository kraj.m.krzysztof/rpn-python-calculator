from enum import Enum

class Calculator(object):
    def __init__(self) -> None:
        self.stack = Stack()
        self.calculation = None
        self.current_value = 0
    
    def add_calculation(self, calculation_str):
        self.calculation = CalculationParser(calculation_str)
    
    def calculate(self):
        if not self.has_calculation:
            print('No calculation has been provided!')
        
        while not self.calculation.parsing_finished:
            next_calculation_part = self.calculation.get_calculation_part()
            calculation_part_type = next_calculation_part.get_type()

            if calculation_part_type == CharType.UNKNOWN:
                print(f'Unknown character {next_calculation_part.value}. Procedure aborted')
                return
            
            elif calculation_part_type == CharType.NUMBER:
                self.stack.push(int(next_calculation_part.value))

            elif calculation_part_type == CharType.OPERAND:
                if not self.check_if_possible_to_perform_calculation:
                    print('There has to be at least 2 values before operand to perform calculation!')
                    return

                self._perform_calculation(next_calculation_part.value)

    def get_next_char(self, current_char_position):
        return self.calculation[current_char_position + 1]
    
    def is_next_char_numeric(self, next_char) -> bool:
        char = Char(next_char)
        return (char.get_type() == CharType.NUMBER)

    def check_if_possible_to_perform_calculation(self):
        return (self.stack.stack_length > 1)

    def _perform_calculation(self, operand):
        b = self.stack.pop()
        a = self.stack.pop()

        if operand == "+":
            result = a + b
        elif operand == '-':
            result = a - b
        elif operand == '*':
            result = a * b
        elif operand == "/":
            result = a / b

        self.stack.push(result)
        self.current_value = result
        
    def has_calculation(self):
        return (self.calculation != None)


class Stack(object):
    def __init__(self) -> None:
        self.stack_length = 0
        self.stack_items = []
    
    def push(self, num):
        self.stack_items.append(num)
        self.stack_length += 1
    
    def pop(self):
        if self.is_empty():
            print('Cannot remove item from empty stack!')

        else:
            self.stack_length -= 1
            return self.stack_items.pop()
    
    def is_empty(self) -> bool:
        return self.stack_length == 0
    
    def current_value(self):
        if self.is_empty():
            print('Stack does not have any items!')

        else:
            return self.stack_items[self.stack_length - 1]

class CalculationParser(object):
    def __init__(self, calculation) -> None:
        self.calculation = calculation
        self.position = 0
        self.total_length =len(calculation)
        self.parsing_finished = False
    
    def get_calculation_part(self):
        current_char = Char(self.calculation[self.position])

        if current_char.get_type() == CharType.NUMBER:
            while self._is_next_char_numeric():
                current_char.append_char(self._get_next_char())
                self.position += 1

        self._update_parsing_status()
        self.position += 1

        return current_char
    
    def _is_next_char_numeric(self):
        next_char = Char(self._get_next_char())
        return (next_char.get_type() == CharType.NUMBER)
    
    def _get_next_char(self):
        return self.calculation[self.position + 1]

    def _update_parsing_status(self):
        self.parsing_finished = (self.position + 1 == self.total_length)


class Char(object):
    def __init__(self, value: str) -> None:
        self.value = value
        self.type = self._check_my_type()

    def get_type(self):
        return self.type
    
    def append_char(self, new_char):
        self.value += new_char
    
    def _check_my_type(self):
        if self.value.isnumeric():
            char_type = CharType.NUMBER

        elif self.value in ['+', '-', '*', '/']:
            char_type = CharType.OPERAND

        elif self.value == ' ':
            char_type = CharType.SPACE

        else:
            char_type = CharType.UNKNOWN

        return char_type
    

class CharType(Enum):
    NUMBER = 1
    OPERAND = 2
    SPACE = 3
    UNKNOWN = 4