from calculator import Calculator

my_calculator = Calculator()

while True:
    print('Please, provide calculation to be solved in a form of RPN')
    user_input = input('>>>')
    if user_input == "":
        print('Provided calculation was:', user_input, sep='\n')
        break
    
    else:
        my_calculator.add_calculation(user_input)
        my_calculator.calculate()
        print(f'{user_input} = {my_calculator.current_value}')
        break

